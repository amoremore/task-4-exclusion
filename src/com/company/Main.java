package com.company;

import java.util.Scanner;

public class Main {
    public  static void main(String[] args) {
        Scanner number = new Scanner(System.in);

        System.out.print("Введите a: ");
        String a = number.nextLine();

        System.out.print("Введите b: ");
        String b = number.nextLine();

        try {

            double resul = vichitanie(a, b);
            System.out.println("Разность: " + resul);

                System.out.println();

            resul = umnojenie(a, b);
            System.out.println("Умножение: " + resul );

                System.out.println();

            resul = slojenie(a, b);
            System.out.println("Сложение: " + resul);

                System.out.println();

            resul = delenie(a, b);
            System.out.println("Деление: " + resul);
        }
        catch (NumberFormatException ex)
        {
            System.out.println("Вводить можно только числа!!!");
        }

    }

    public static double vichitanie (String a,String b){
        double first = Double.valueOf(a);
        double second = Double.valueOf(b);
        return first-second;
    }

    public static double umnojenie (String a, String b){
        double first = Double.valueOf(a);
        double second = Double.valueOf(b);
        return first*second;
    }

    public static double slojenie (String a, String b){
        double first = Double.valueOf(a);
        double second = Double.valueOf(b);
        return first+second;
    }

    public static double delenie (String a, String b){
        double first = Double.valueOf(a);
        double second = Double.valueOf(b);
        return first/second;
    }
}